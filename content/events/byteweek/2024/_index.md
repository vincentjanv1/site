---
startdate: 2024-01-29
enddate: 2024-02-03
allday: true
linktitle: "Byteweek 2024"
title: "Byteweek 2024"
location: "HSBXL"
eventtype: "5 days of awesomeness"
price: ""
image: "byteweek.png"
series: "byteweek2024"
start: "true"
aliases: [/byteweek]
---

The week before Fosdem conference, HSBXL compiles a week of hacking.  
bin(111) days of hackatons, workshops and talks.  
We end our week with the notorious [Bytenight](/bytenight)!

Invite your FOSS group over!
We are opening our doors for the FOSS community to hack on their projects in the advent of FOSDEM. There’s heating, food, WiFi and also our tool corner to get creative. There’s a setup to give presentations, do workshops or even stream to your community if you feel inclined to do so. Reach out at for more details!

# Monday January 29th

{{< events series="byteweek2024" when="2024-01-29" showtime="true" showallday="true" >}}

# Tuesday January 30th

{{< events series="byteweek2024" when="2024-01-30" showtime="true" showallday="true" >}}

# Wednesday January 31st

{{< events series="byteweek2024" when="2024-01-31" showtime="true" showallday="true" >}}

# Thursday February 1st

{{< events series="byteweek2024" when="2024-02-01" showtime="true" showallday="true" >}}

# Friday February 2nd

{{< events series="byteweek2024" when="2024-02-02" showtime="true" showallday="true" >}}

# Saturday February 3rd

{{< events series="byteweek2024" when="2024-02-03" showtime="true" showallday="true" >}}

# Call for participation

If you want to do a talk, a workshop or have a hackathon,  
send a mail to **contact@hsbxl.be** with your proposal.
