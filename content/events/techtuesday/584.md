---
techtuenr: "584"
startdate: 2021-07-06
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue584
series: TechTuesday
title: TechTuesday 584
linktitle: "TechTue 584"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
