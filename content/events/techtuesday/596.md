---
techtuenr: "596"
startdate: 2021-09-28
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue596
series: TechTuesday
title: TechTuesday 596
linktitle: "TechTue 596"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
