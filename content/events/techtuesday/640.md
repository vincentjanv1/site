---
techtuenr: "640"
startdate: 2022-08-02
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue640
series: TechTuesday
title: TechTuesday 640
linktitle: "TechTue 640"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.

Let's drink a good one on Bill Gates, who once thought that 640KB of RAM would be more than a computer would ever need.