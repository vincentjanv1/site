---
techtuenr: "674"
startdate: 2023-03-28
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue674
series: TechTuesday
title: TechTuesday 674
linktitle: "TechTue 674"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
