---
eventid: techtue522
startdate:  2019-05-14
starttime: "19:00"
linktitle: "TechTue 522"
title: "TechTuesday 522"
price: ""
image: "techtuesday.jpg"
series: "TechTuesday"
eventtype: "Social get-together"
location: "HSBXL"
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
