---
techtuenr: "657"
startdate: 2022-11-29
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue653
series: TechTuesday
title: TechTuesday 657
linktitle: "TechTue 657"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
