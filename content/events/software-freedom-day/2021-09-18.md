---
startdate:  2021-09-18
starttime: "10:00"
endtime: "19:00"
linktitle: "Software Freedom Day 2021"
title: "Software Freedom Day 2021"
price: "Free"
image: "sfd.png"
series: "SFD"
eventtype: "workshops & talks"
location: "HSBXL"
---

During Software Freedom Day, we'll be celebrating the merits of Free software (free as in Freedom, we ask you to pay for your beer)

These are already confirmed: (timeslots can change)
* 13:00 [SmellyHTTP? HeyGemini!](#jonny) (Jonny)
* 14:00 [Why (and how to) switch to OpenStreetMap for your website](#osm) (OSM Belgium).
* 15:00 [FreeCAD part design 101](#kai) (Kai)
* 16:00 [FreeCAD programming](#yorik) (Yorik)
* 17:00 [Using Inkscape to make a design on the GlowForge (laser cutter)](#jurgen) (Jurgen)
* 19:00 [Setting up OBS to record an event](#wouter) (this setup will be used on the day itself too) (Wouter)

## SmellyHTTP? HeyGemini! {#jonny}
Stop groping for your ad blocker.
Step away from the Javascript hooha.
Deprioritize the visual clutter.
Gemini should cocoa!

The minimalism of the Gemini protocol and its terse Markdown format, Gemtext is exciting. It reapproaches the early principles of circa 1993 HTTP standards and ensures transmission is securely provided.

It is modern because it is:
* Easy to understand;
* Accessible to interact with (whether as a consumer or a contributor);
* and treats privacy as a foremost priority.

An introductory talk, the foundational principles will be explained and interesting tooling will be presented. Special emphasis will be on how programmers get creative managing Gemini purposeful limitations.

**Requirements**: none

**Level**: All welcome

**Recording**:
{{< youtube Nmm0ZY3zI6E >}}

## Why (and how to) switch to OpenStreetMap for your website {#osm}
What's OpenStreetMap? How does it work? How is it different from every other solution out there? And why would you use it? These and many more questions in a practical introduction on this wonderful tool!

**Level**: beginner friendly.

**Recording**:
{{< youtube OHfmnBD1r18 >}}

## FreeCAD Part Design 101 {#kai}
From sketch to 3D printable part.

We’ll be going over the basics of creating a sketch, turning it into a 3D object and using different tools to transform it into a printable part.

**Requirements**: Computer with FreeCAD installed

**Level**: beginner friendly.

**Slides**:
 - [PDF format](slides/SFD_FreeCAD101.pdf)
 - [LibreOffice ODP format](slides/SFD_FreeCAD101.odp)

**Recording**
{{< youtube V-6_E8prxLI >}}

## FreeCAD programming {#yorik}
FreeCAD is a well known 3D modelling platform, but its real power lies in how easily it can be programmed and extended with very simple Python scripts. Come discover and practice a few things with us!

**Requirements**: Your computer with FreeCAD installed - No programming experience needed.

**Level**: basic understanding FreeCAD.

**Notes**: https://yorik.uncreated.net/blog/2021-019-workshop-hsbxl

**Recording**
{{< youtube O1hYBgk_aLY >}}

## Using Inkscape to make a design for the laser cutter (GlowForge) {#jurgen}
We have an easy to use Laser Cutter in the hacker space, that works with SVG-files.
Let's create a simple Raspberry Pi case, Arduino or other custom designer enclosure to carry your next project.

**Requirements**: Bring along your computer, make sure you have Inkscape installed.
We'll be starting assuming no knowledge of Inkscape.

**Level**: beginner friendly.

## Setting up OBS to record (or live stream) an event {#wouter}
OBS is a software based live video mixer that allows working with scenes to quickly switch between differen layouts and video sources. with this we can make a small local mixing desk, allowing us to record videos in such a way that there is no more mixing needed.
we'll be using OBS to record the talks during the day, and using this setup as a starting point, but end up at a setup where no extra hardware is needed.

**Requirements**:Bring along your computer, make sure you have obs installed.
We'll be starting assuming no knowledge of OBS

**Level**: beginner friendly.
