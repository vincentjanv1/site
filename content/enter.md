---
title: "How to get to HSBXL?"
linktitle: "How to get to HSBXL?"
---

**Address:** Rue Osseghem 53, 1080 Molenbeek, Brussels, Belgium.

# We are now located in LionCity

HSBXL has moved to LionCity, a vibrant and innovative space in the heart of Brussels. Situated in the former Delhaize distribution centre in Molenbeek, LionCity is home to a diverse mix of crafts, entrepreneurs, social projects, culture, sports, and urban agriculture.

For more details on LionCity, visit [here](https://www.entrakt.be/en/lioncity).

# How to get to the address

{{< image src="/images/enter_space/hsbxl_lioncity.png" >}}

### For assistance getting in

- **Chat**: Join us at [Matrix chat](https://matrix.to/#/#hsbxl:matrix.org). Preferred for writing ahead of time.
- **Phone**: [+32 2 880 40 04](tel:+3228804004). Available when members are present in the space.

## By public transport 🚆

You can get to Brussels North Train Station using trains, or take the metro to Beekkant.

**Entrance Instructions**
Upon reaching LionCity, a member will guide you from there.
(Detailed entrance instructions will be provided in a few weeks.)

## By car 🚘

Parking is possible in the streets, or in the parking building nearby. (Detailed parking instructions will be provided in a few weeks.)

## Shared mobility

Brussels has a [vast offering in shared mobility](https://www.brussels.be/alternative-mobility). Use a bike, step, or even car using the appropriate plans.

Keep in mind that Brussels has enforced drop zones for shared mobility bikes, electric scooters, and kick scooters (aka trottinette or step). Please park responsibly!

## At night: Collecto

To get back to your home/hotel, there is [Collecto](https://en.collecto.be/). A Taxi picks you up at certain points (many STIB public transport stops) and brings you to your destination within the Brussels region for the fixed price of 6 euro.

You can order a Collecto at any time between 11 p.m. and 6 a.m, but it is advisable to order at least 20 minutes in advance. If you have a smartphone, you can use the [Android](https://play.google.com/store/apps/details?id=be.tradecom.collecto&hl=en&gl=US) or [Apple iOS](https://apps.apple.com/be/app/collecto-your-shared-taxi/id921558166) app, otherwise, you can call +32 2 800 36 36 to reserve your Collecto.

There is a [collecto](https://taxisverts.be/en/collecto-en/) startpoint at the entrance of Beekant STIB metro station, at 100m away.

# How to get to the Hackerspace once you have reached LionCity

Until the end of march go to the big black gate and signt to the guard in the guardhouse.
You will need to register arrival and exit to security. (note that this is a temporary situation until the end of the month of March 2024)
after exiting the guard house go to the left and follow the building until you encounter a roofed street. Enter the street, the space is the second door on the right inside the building, and then the second door to the right in the hallway.

## Go to the general location

The general location of the hackerspace is in the Molenbeek neighbourhood.

(More detailed entrance instructions will follow in a few weeks.)

---
